const {getUsers, getPromiseArray, printTasks} = require('./api');

window.addEventListener('load',()=>{
    getUsers().then(respsonse => respsonse.json())
    .then(users => {
        let promiseArray = getPromiseArray(users);
        return Promise.all([Promise.all(promiseArray),users]) })
    .then(([responses,users]) => Promise.all([responses.map(response => response.json()),users]))
    .then(([responses,users]) => {
        let promiseArray = Promise.all(responses);
        return Promise.all([promiseArray,users])
    })
    .then(([responses,users]) => responses.map(response=> printTasks(response,users)) )
    .catch(err =>{
        console.error("error occured in: "+ err.message);
        console.error(err.stack);
    })
})