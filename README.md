# Promises on browser 2

* Users API url: https://jsonplaceholder.typicode.com/users
* Todos API url: https://jsonplaceholder.typicode.com/todos

* Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
* Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

On browser I should be able to see the todos with the user's name prefixed with @. For example it looks like this in HTML using Checkbox:
 [  ] @Mayank Bring Mercedes Benz
 [  ] @siva Buy a BMW X7
 [  ] @Venkateshwar Buy Bugatti Chiron Sport 

## This is my output for the above drill

![](Images/output.png)