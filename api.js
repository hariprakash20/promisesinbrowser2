let mainContainer = document.querySelector('.main-container');

const getUsers = () => fetch("https://jsonplaceholder.typicode.com/users");

const getPromiseArray = users => users.map(user => fetch("https://jsonplaceholder.typicode.com/todos?userId="+user.id))

const renderList = (userName,id, completed, title) => {
    return `<div class="task">
    <input type="checkbox" id="${id}" ${completed? 'checked="checked"' : ''}">
    <p class="title"><b class="name">@${userName}</b> ${title}</p>
</div>`
}

function printTasks(tasks, users){
    users.map(user => {
        if(user.id == tasks[0].userId){
            userName = user.name;
        } 
    })
    mainContainer.innerHTML += `<div class="user-tasks">`
    tasks.map(task=> {
        mainContainer.innerHTML += renderList(userName,task.id, task.completed, task.title);
    });
    mainContainer.innerHTML += `</div>`
}

module.exports = {getUsers, getPromiseArray, printTasks};